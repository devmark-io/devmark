// 3rd party objects
const koa = require("koa"),
      serve = require("koa-static"),
      pug = require('koa-pug'),
      http = require('http'),
      convert = require('koa-convert');

// in app objects
const assets = require('./assets.json'),
      router = require('./router.js');

// exports
const app = new koa();

// environment shortcuts
const PORT = process.env.PORT,
      TARGET = process.env.NODE_ENV,
      DEBUG = process.env.DEBUG;

new pug({
  viewPath: __dirname + '/views',
  debug: DEBUG,
  pretty: true,
  compileDebug: true,
  locals: assets,
  app: app
});

// general routing
app.use(router.unsecured.routes());

// static routing
app.use(serve(__dirname + "/public"));

const server = module.exports = http.createServer(app.callback());
server.listen(PORT);
console.log("devmark:" + TARGET + " has started listening on " + PORT);

process.on('exit', function() {
  console.log("devmark:" + TARGET + " has stopped listening on " + PORT);
});
