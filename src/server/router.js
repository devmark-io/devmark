// 3rd party objects
var router = require("koa-router");

module.exports.unsecured = new router()
  .get('/', async function(ctx) {
    await ctx.render('index', {});
  })
  .get('/privacy', async function(ctx) {
    await ctx.render('privacy', {});
  })
  .get('/chrome-extension-release-notes', async function(ctx) {
    await ctx.render('chrome-extension-release-notes', {});
  });
