/**
 * Created by peadar on 18/11/16.
 */
$(document).ready(function(){
  $('.slidey').slick({
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 3000
  });
});