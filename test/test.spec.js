var app = require("../target/app.js");
var http = require("http");
var request = require("co-supertest").agent(app);

describe("devmark http api", function() {

  afterEach(function*() {
    app.close();
  });

  it("can access home page", function*() {
    yield request
      .get('/')
      .set("Accept", "text/html")
      .expect(200)
      .expect("Content-type", "text/html; charset=utf-8")
      .end();
  });
});
