const gulp = require('gulp'),
      mocha = require('gulp-mocha-co'),
      gutil = require('gulp-util'),
      env = require('gulp-env'),
      nodemon = require('gulp-nodemon'),
      less = require('gulp-less'),
      path = require('path'),
      rev = require('gulp-rev'),
      es = require('event-stream'),
      del = require('del'),
      Q = require('q');
      concatCss = require('gulp-concat-css'),
      LessPluginCleanCSS = require('less-plugin-clean-css'),
      LessPluginAutoPrefix = require('less-plugin-autoprefix'),
      cleancss = new LessPluginCleanCSS({ advanced: true }),
      autoprefix= new LessPluginAutoPrefix({ browsers: ["last 2 versions"] });

var paths = {
  scripts: ['src/client/scripts/**/*.js'],
  styles: ['src/client/styles/**/*.less'],
  views: ['src/server/views/**/*.pug'],
  images: ['src/client/images/**/*']
};

gulp.task('watch', function() {
  gulp.watch(paths.scripts);
  gulp.watch(paths.styles);
  gulp.watch(paths.views);
  gulp.watch(paths.images);
});

gulp.task('set-test-env', async function () {
  await env({
    file: ".env.test.json",
    vars: {
      //any vars you want to overwrite
    }
  });
});

gulp.task('set-dev-env', async function () {
  await env({
    file: ".env.dev.json",
    vars: {
      //any vars you want to overwrite
    }
  });
});

gulp.task('target-clean', async function () {
    await del(['target/*']);
});

gulp.task('target-server', function () {
  return gulp.src(['src/server/**/*'])
    .pipe(gulp.dest('target/'));
});

gulp.task('target-client', async function () {
  var styles = gulp.src('src/client/styles/index.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ],
      plugins: [autoprefix, cleancss]
    }))
    .pipe(concatCss("bundle.css"))
    .pipe(gulp.dest('target/public'));

  var scripts = gulp.src('src/client/scripts/*.js')
    .pipe(gulp.dest('target/public'));

  var html = gulp.src('src/client/*.html')
    .pipe(gulp.dest('target/public'));

  var images = gulp.src(['src/client/images/*.jpg', 'src/client/images/*.png', 'src/client/images/*.ico'])
    .pipe(gulp.dest('target/public'));

  return await es.merge(styles, scripts, html, images)
    .pipe(rev())
    .pipe(gulp.dest('target/public'))
    .pipe(rev.manifest('../assets.json'))
    .pipe(gulp.dest('target/public'));
});

gulp.task('mocha', async function () {
  await gulp.src(['test/**.js'], { read: true })
    .pipe(mocha({reporter: 'nyan'}));
});

gulp.task('watch-mocha', function() {
  gulp.watch(['src/**', 'test/**'], ['mocha']);
});


gulp.task('target-start', async function () {
  await nodemon({
    script: 'target/app.js',
    ext: 'js css less'
  }).on('restart', function () {
    console.log('restarted!');
  })
});

gulp.task('test', gulp.series('set-test-env', 'target-client', 'watch-mocha', 'mocha'));
gulp.task('build', gulp.series('target-clean', 'target-server', 'target-client'));
gulp.task('start', gulp.series('set-dev-env', 'build', 'target-start'));
gulp.task('local', gulp.series('set-dev-env', 'build', 'target-start', 'watch'));

gulp.task('noop', function(){});
